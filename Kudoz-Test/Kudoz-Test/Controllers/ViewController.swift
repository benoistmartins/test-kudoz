//
//  ViewController.swift
//  Kudoz-Test
//
//  Created by Benoist Martins on 15/03/2018.
//  Copyright © 2018 benoist.martins. All rights reserved.
//

import UIKit
import Alamofire
import PKHUD
import Reachability

class ViewController: UITableViewController {
    
    //MARK: Properties
    var photos = [Photo]()
    var sections = [Int: [Photo]]()
    var sectionKeys = [Int]()
    var pics = [Photo]()
    var startIndex = 0
    let reachability = Reachability()!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let appName = Bundle.main.infoDictionary!["CFBundleName"] as! String
        
        self.title = appName
        
        // Launch the request
        fetchPhotos(start: startIndex)
        
        do {
            try reachability.startNotifier()
        } catch {
            print("Unable to start notifier")
        }
    }
    
    //MARK: - Table View
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return sectionKeys.count
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (sections[sectionKeys[section]]?.count)!
    }
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 50
    }
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return "Album #\(sectionKeys[section])"
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! PhotoTableViewCell
        
        // The current object is the one at the indexPath.row
        let photo = sections[sectionKeys[indexPath.section]]![indexPath.row]
        
        // Display image using AlamofireImage
        if let photoThumbnail = photo.photoThumbnailUrl {
            cell.photoImageView?.image = UIImage(contentsOfFile: photoThumbnail)
        } else {
            cell.photoImageView?.image = UIImage(named:"Placeholder")
        }
        
        cell.photoIdLabel.text = "Photo #\(photo.photoId)"
        cell.photoTitleLabel.text = photo.title
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 110
    }
    
    override func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        
        //Bottom Refresh
        if scrollView == tableView{
            
            if ((scrollView.contentOffset.y + scrollView.frame.size.height) >= scrollView.contentSize.height)
            {
                startIndex = startIndex + Constants.WebservicesParameters.limit
                
                do {
                    try reachability.startNotifier()
                } catch {
                    print("Unable to start notifier")
                }
                
                fetchPhotos(start: startIndex)
            }
        }
    }
    
    
    //MARK: Private Methods
    func fetchPhotos(start: Int) {
        HUD.show(.progress)
        
        if (reachability.connection == .cellular || reachability.connection == .wifi) {
            PhotoManager.getPhoto(start: start){ (photos) in
                self.photos = photos.sorted(by: { $0.photoId < $1.photoId })
                
                self.groupPhotos(pictures: self.photos)
                
                HUD.flash(.success, delay: 1.0)
                self.tableView.reloadData()
            }
        }
        else {
            PhotoManager.getPhotoOffline(start: start){ (photos) in
                self.photos = photos.sorted(by: { $0.photoId < $1.photoId })
                
                self.groupPhotos(pictures: self.photos)
                
                HUD.flash(.success, delay: 1.0)
                self.tableView.reloadData()
                
            }
        }
    }
    
    func groupPhotos(pictures : [Photo]) {
        for picture in pictures {
            let albumId = Int(picture.albumId)
            
            if(sections[albumId] == nil) {
                sectionKeys.append(albumId)
                pics.removeAll()
                pics.append(picture)
                sections[albumId] = pics
            } else {
                pics.append(picture)
                sections[albumId] = pics
            }
        }
    }
    
}
