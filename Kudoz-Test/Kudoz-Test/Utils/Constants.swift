//
//  Constants.swift
//  Kudoz-Test
//
//  Created by Benoist Martins on 15/03/2018.
//  Copyright © 2018 benoist.martins. All rights reserved.
//

import UIKit

struct Constants {
    struct WebservicesParameters {
        // URL API
        static let url = "http://jsonplaceholder.typicode.com/photos"
        static let limit = 100
    }
}
