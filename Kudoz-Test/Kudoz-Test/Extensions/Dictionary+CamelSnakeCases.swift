//
//  Dictionary+CamelSnakeCases.swift
//  Kudoz-Test
//
//  Created by Benoist Martins on 15/03/2018.
//  Copyright © 2018 benoist.martins. All rights reserved.
//

import UIKit

import Foundation

// MARK: - Camel and snake cases

extension Dictionary {
    
    func keysToCamelCase() -> Dictionary {
        
        var result: Dictionary = [:]
        
        for (key, value) in self {
            var newKey = key
            var newValue = value
            
            // Array value
            if let v = newValue as? Array<Any> {
                var array: [Any] = []
                for elm in v {
                    if let elmDict = elm as? Dictionary, let camelelmDict = elmDict.keysToCamelCase() as? Value {
                        array.append(camelelmDict)
                    }
                    else {
                        array.append(elm)
                    }
                }
                
                if let arrayValue = array as? Value {
                    newValue = arrayValue
                }
            }
            
            // Dictionary value
            if let v = newValue as? Dictionary, let camelValue = v.keysToCamelCase() as? Value {
                newValue = camelValue
            }
            
            // String Key
            if let k = newKey as? String, let camelKey = k.toCamelCase as? Key {
                newKey = camelKey
            }
            
            result[newKey] = newValue
        }
        
        return result
    }
    
    func keysToSnakeCase() -> Dictionary {
        let keys = Array(self.keys)
        let values = Array(self.values)
        var result: Dictionary = [:]
        
        keys.enumerated().forEach { ( index, key) in
            var newKey = key
            var newValue = values[index]
            
            if let v = newValue as? Dictionary, let snakeValue = v.keysToSnakeCase() as? Value {
                newValue = snakeValue
            }
            
            if let k = key as? String, let snakeKey = k.toSnakeCase as? Key {
                newKey = snakeKey
            }
            
            result[newKey] = newValue
        }
        
        return result
    }
}
