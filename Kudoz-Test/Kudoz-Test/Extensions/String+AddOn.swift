//
//  String+AddOn.swift
//  Kudoz-Test
//
//  Created by Benoist Martins on 15/03/2018.
//  Copyright © 2018 benoist.martins. All rights reserved.
//

import UIKit

// MARK: Camel and snake case

extension String {
    
    var toCamelCase: String {
        var camelCase = ""
        let items = self.components(separatedBy: "_")
        items.enumerated().forEach {
            camelCase += 0 == $0 ? $1 : $1.capitalized
        }
        
        return camelCase
    }
    
    var toSnakeCase: String {
        var snakeCase = ""
        for char in self {
            
            if "A"..."Z" ~= char {
                snakeCase.append("_")
            }
            snakeCase.append(String(char).lowercased())
        }
        
        return snakeCase
    }
}
