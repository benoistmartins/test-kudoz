//
//  Router.swift
//  Kudoz-Test
//
//  Created by Benoist Martins on 15/03/2018.
//  Copyright © 2018 benoist.martins. All rights reserved.
//

import Foundation
import Alamofire

protocol Router: URLRequestConvertible {
    
    static var baseUrl: String { get }
    var method: HTTPMethod { get }
    var params: Json? { get }
}

extension Router {
    
    // MARK: - URLRequestConvertible delegate
    
    func asURLRequest() throws -> URLRequest {
        let requestParams: Json? = params
        let url = try type(of: self).baseUrl.asURL()
        
        var urlRequest = URLRequest(url: url)
        urlRequest.httpMethod = method.rawValue
        
        urlRequest = try URLEncoding.default.encode(urlRequest, with: requestParams)
        
        return urlRequest
    }
}

enum ApiRouter: Router {
    
    static var baseUrl: String {
        return Constants.WebservicesParameters.url
    }
    
    case getPhoto(start: Int)
    
    var method: HTTPMethod {
        switch self {
            
        case .getPhoto:
            return .get
        }
    }
    
    var params: Json? {
        switch self {
            
        case .getPhoto(let start) :
            return  ["_start"          :   start,
                     "_limit"          :   Constants.WebservicesParameters.limit]
        }
    }
}
