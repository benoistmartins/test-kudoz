//
//  Api.swift
//  Kudoz-Test
//
//  Created by Benoist Martins on 15/03/2018.
//  Copyright © 2018 benoist.martins. All rights reserved.
//

import Foundation
import Alamofire

typealias AlamofireResponse = DataResponse<Any>
typealias Json = [String : Any]

class ApiManager: NSObject {
    
    // MARK: - Properties
    
    static var currentTask: DataRequest?
    
    static var sessionManager = defaultManager()
    
    // MARK: - Session configuration
    
    private static func defaultManager() -> SessionManager {
        let configuration = URLSessionConfiguration.default
        configuration.timeoutIntervalForRequest = 20
        configuration.timeoutIntervalForResource = 20
        
        return Alamofire.SessionManager(configuration: configuration)
    }
    
    // MARK: - Request
    
    static func request(url: URLRequestConvertible, loading: Bool = true, completion: @escaping (Json) -> ()) {
        if loading {
        }
        
        currentTask = sessionManager.request(url).validate().responseJSON { (response) in
            if let responseValue = response.value {
                if let value = responseValue as? [Json] {
                    for json in value {
                        completion(json.keysToCamelCase())
                    }
                }
            }
        }
    }
    
}
