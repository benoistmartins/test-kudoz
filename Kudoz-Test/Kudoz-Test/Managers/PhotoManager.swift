//
//  PhotoManager.swift
//  Kudoz-Test
//
//  Created by Benoist Martins on 15/03/2018.
//  Copyright © 2018 benoist.martins. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireImage

class PhotoManager: NSObject {
    
    static func getPhoto(start: Int, completion: @escaping([Photo]) -> Void) {
        let apiEndPoint = ApiRouter.getPhoto(start: start)
        
        ApiManager.request(url: apiEndPoint) { (jsonResponse) in
            
            // Create an empty array
            var photos: [Photo] = []
            
            // Get the photo albumId
            var photoAlbumId = Int()
            if let albumId = jsonResponse["albumId"] as? Int {
                photoAlbumId = albumId
            }
            
            // Get the photo id
            var photoId = Int()
            if let picId = jsonResponse["id"] as? Int {
                photoId = picId
            }
            
            // Get the photo title
            var photoTitle = String()
            if let picTitle = jsonResponse["title"] as? String {
                photoTitle = picTitle
            }
            
            // Get the photo thumbnailUrl
            var photoThumbnailUrl = String()
            if let picThumbnailUrl = jsonResponse["thumbnailUrl"] as? String {
                photoThumbnailUrl = picThumbnailUrl
            }
            
            // Get access to shared instance of the file manager
            let fileManager = FileManager.default
            
            // Get the url for the users home directory
            let documentsUrl = fileManager.urls(for: .documentDirectory, in: .userDomainMask).first!
            
            // Get the document url as a string
            let documentPath = documentsUrl.path
            
            // Create filepath url by appending final path component
            let filePath = documentsUrl.appendingPathComponent("\(String(photoId)).png")
            
            // Check for existing image data
            do {
                // Look through array of files in documentDirectory
                let files = try fileManager.contentsOfDirectory(atPath: "\(documentPath)")
                
                for file in files {
                    // If we find existing image filePath delete it to make way for new imageData
                    if "\(documentPath)/\(file)" == filePath.path {
                        try fileManager.removeItem(atPath: filePath.path)
                    }
                }
            } catch {
                print ("Could not add image from document directory : \(error)")
            }
            
            Alamofire.request(photoThumbnailUrl).responseImage { response in
                if let image = response.result.value {
                    do {
                        if let pngImageData = UIImagePNGRepresentation(image) {
                            try pngImageData.write(to:filePath, options: .atomic)
                        }
                    } catch {
                        print("couldn't write image")
                    }
                }
            }
            
            let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
            let photo = Photo(context: context) // Link Task & Context
            
            photo.albumId = Int64(photoAlbumId)
            photo.photoId = Int64(photoId)
            photo.title = photoTitle
            photo.photoThumbnailUrl = filePath.path
            
            // Array with all Photo
            photos.append(photo)
            
            // return array
            completion(photos)
        }
    }
    
    static func getPhotoOffline(start: Int, completion: @escaping([Photo]) -> Void) {
        // Create an empty array
        var photos: [Photo] = []
        let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
        do {
            photos = try context.fetch(Photo.fetchRequest())
        } catch {
            print("Fetching Failed")
        }
        // return array
        completion(photos)
    }
}
