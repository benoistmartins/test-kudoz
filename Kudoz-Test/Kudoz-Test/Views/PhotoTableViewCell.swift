//
//  PhotoTableViewCell.swift
//  Kudoz-Test
//
//  Created by Benoist Martins on 15/03/2018.
//  Copyright © 2018 benoist.martins. All rights reserved.
//

import UIKit

class PhotoTableViewCell: UITableViewCell {

    //MARK: IBOUtlet
    @IBOutlet weak var photoImageView: UIImageView!
    @IBOutlet weak var photoIdLabel: UILabel!
    @IBOutlet weak var photoTitleLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

}
